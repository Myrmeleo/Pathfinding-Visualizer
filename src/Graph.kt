
/*
Empty:  A* pathfinder can search this node
Start:  pathfinder starts here
Goal:   pathfinder stops after reaching this node
Wall:   pathfinder cannot go through here
 */
enum class NodeType {
    EMPTY, START, GOAL, WALL
}


/*
A Node is a step in a path
    A Node can be the start, end, or a middle point in a path
    A Node is connected to other Nodes and has a cost associated with traveling to it
Params:
    the coordinates starting from (0,0)
    the graph this node belongs to
*/
class Node (val x: Int, val y: Int, private val graph : Graph) {
    var type = NodeType.EMPTY
    //diagonal nodes are included in neighbours
    var neighbours: MutableList<Node> = mutableListOf()
    //used when visualizer traces the path backwards from the goal to the start; works like a linked list
    var previous: Node? = null
    /*
    distance from start to this node
     */
    var gDist: Float = 0f
    /*
    fDist = g + h where
    g = distance from start to this node
    h = heuristic guess of distance from this node to the goal
    heuristic distance function is given in AStar.kt
     */
    var fDist: Float = 0f

    /*
    for animation purposes, we will track 2 things:
    whether the node has or has not been looked at before
    whether this node is on the optimal path from start to goal
    these variables are not used in the pathfinding algorithm
     */
    var wasChecked: Boolean = false
    var isOnPath: Boolean = false

    //add all neighbouring Nodes to neighbours
    //a Node is a neighbour if it exists within one tile
    fun addNeighbours() {
        for (relativeX in -1..1) {
            for (relativeY in -1..1) {
                if      (x + relativeX >= 0 && x + relativeX < graph.size
                        && y + relativeY >= 0 && y + relativeY < graph.size
                        && !(relativeX == 0 && relativeY == 0))
                {
                    neighbours.add(graph.getNode(x + relativeX, y + relativeY))
                }
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        return other is Node && x == other.x && y == other.y
    }

    fun clone() : Node {
        val copy = Node(this.x, this.y, this.graph)
        copy.addNeighbours()
        copy.type = this.type
        return copy
    }
}


/*
A Graph is a collection of traversable Nodes
    It consists of a collection of Nodes and the relations each Node has to the others
Param
    the number of rows and columns it has
 */
class Graph (val size: Int) {
    //populates a list with columns of Nodes, result is a 2-D size*size list of Nodes
    val grid = List(size) { x -> List(size) {y -> Node(x, y, this)}}
    var start: Node = grid[0][0]
    var goal: Node = grid[size - 1][size - 1]

    var isBeingTraversed = false

    init {
        //after all Nodes are instantiated, tell each Node to find its neighbours
        for (i in grid) {
            for (j in i) {
                j.addNeighbours()
            }
        }
        //mark start and goal
        start.type = NodeType.START
        goal.type = NodeType.GOAL
    }

    fun getNode(x: Int, y: Int) : Node {
        return grid[x][y]
    }

    fun setStart(x: Int, y: Int) {
        if (x >= 0 && x < size && y >= 0 && y < size) {
            val target = grid[x][y]
            if (target.type != NodeType.GOAL && target.type != NodeType.WALL) {
                start.type = NodeType.EMPTY
                target.type = NodeType.START
                start = target
            }
        }
    }

    fun setGoal(x: Int, y: Int) {
        if (x >= 0 && x < size && y >= 0 && y < size) {
            val target = grid[x][y]
            if (target.type != NodeType.START && target.type != NodeType.WALL) {
                goal.type = NodeType.EMPTY
                target.type = NodeType.GOAL
                goal = target
            }
        }
    }

    /*
    for drawing walls
    we will make the mouse draw a 2x2 area, to prevent the risk of accidentally letting the pathfinder go diagonally
     */
    fun setWall(x: Int, y: Int) {
        for (areaX in x..x+1) {
            for (areaY in y..y+1) {
                if (areaX >= 0 && areaX < size && areaY >= 0 && areaY < size) {
                    val target = grid[areaX][areaY]
                    if (target.type != NodeType.START && target.type != NodeType.GOAL) {
                        target.type = NodeType.WALL
                    }
                }
            }
        }
    }

    fun setEmpty(x: Int, y: Int) {
        if (x >= 0 && x < size && y >= 0 && y < size) {
            val target = grid[x][y]
            if (target.type != NodeType.START && target.type != NodeType.GOAL) {
                target.type = NodeType.EMPTY
            }
        }
    }

    //revert all nodes to their original types
    fun reset() {
        for (i in grid) {
            for (j in i) {
                j.type = NodeType.EMPTY
            }
        }
        start = grid[0][0]
        start.type = NodeType.START
        goal = grid[size - 1][size - 1]
        goal.type = NodeType.GOAL
        clearPath()
        isBeingTraversed = false
    }

    //when the pathfinder resets, each node forgets whether it's been checked or on the path
    fun clearPath() {
        for (i in grid) {
            for (j in i) {
                j.wasChecked = false
                j.isOnPath = false
            }
        }
    }
}
