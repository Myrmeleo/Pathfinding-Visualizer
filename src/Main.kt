import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import java.util.concurrent.TimeUnit
import javax.swing.*
import kotlin.system.measureNanoTime


//make the graph and the panel to draw it
const val graphSize = 40
const val frameSize = 800
val graph = Graph(graphSize)
val drawing = Drawing(graph, frameSize)

//make a toolbar with the buttons to modify nodes
val btnStart = JToggleButton("Place Start")
val btnGoal = JToggleButton("Place Goal")
val btnWall = JToggleButton("Draw Wall")
val btnErase = JToggleButton("Erase Wall")
val buttonFont = Font("Droid Sans", Font.BOLD, 15)
val actionGroup = ButtonGroup()
val btnReset = JButton("Reset")
val btnRun = JButton("Run")

//label to display info about the path, such as the distance from start to goal
val lblInfo = JLabel("Click \'Run\' to start the pathfinder")
val labelFont = Font("Droid Sans", Font.PLAIN, 15)

val toolbar = JToolBar()
val window = JFrame("A* Visualizer")

//pathfinding algorithm with a timer to show steps over time
const val timerDelay = 10
var pathfinder = AStar(graph)
var stepTimer = Timer(timerDelay, null)

//construct the swing components that will comprise the UI
fun buildUI() {
    //make buttons
    btnStart.preferredSize = Dimension(100,50)
    btnStart.font = buttonFont
    btnGoal.preferredSize = Dimension(100,50)
    btnGoal.font = buttonFont
    btnWall.preferredSize = Dimension(100,50)
    btnWall.font = buttonFont
    btnErase.preferredSize = Dimension(100,50)
    btnErase.font = buttonFont
    btnReset.preferredSize = Dimension(100,50)
    btnReset.font = buttonFont
    btnRun.preferredSize = Dimension(100,50)
    btnRun.font = buttonFont
    actionGroup.add(btnStart)
    actionGroup.add(btnGoal)
    actionGroup.add(btnWall)
    actionGroup.add(btnErase)
    //informational label
    lblInfo.font = labelFont
    lblInfo.horizontalAlignment = SwingConstants.CENTER
    lblInfo.background = Color.WHITE
    lblInfo.isOpaque = true
    //add buttons ot toolbar
    toolbar.layout = FlowLayout()
    toolbar.add(btnStart)
    toolbar.add(btnGoal)
    toolbar.add(btnWall)
    toolbar.add(btnErase)
    toolbar.addSeparator()
    toolbar.add(btnReset)
    toolbar.addSeparator()
    toolbar.add(btnRun)
    toolbar.addSeparator()
    toolbar.isFloatable = false
    toolbar.isVisible = true
    //format the label

    //make the window
    window.size = Dimension(813,940)
    window.layout = BorderLayout()
    window.isResizable = false
    window.defaultCloseOperation = JFrame.EXIT_ON_CLOSE;
    window.isVisible = true
    //add components to window
    window.add(toolbar, BorderLayout.NORTH)
    window.add(lblInfo, BorderLayout.CENTER)
    window.add(drawing, BorderLayout.SOUTH)
}


//give listeners to the buttons
fun addButtonFunctions() {
    btnStart.addActionListener{
        drawing.currentAction = Action.CHOOSE_START
    }
    btnGoal.addActionListener{
            drawing.currentAction = Action.CHOOSE_GOAL
    }
    btnWall.addActionListener{
            drawing.currentAction = Action.DRAW_WALL
    }
    btnWall.isSelected = true
    btnErase.addActionListener{
            drawing.currentAction = Action.ERASE
    }
    btnReset.addActionListener{
            graph.reset()
            pathfinder = AStar(graph)
            stepTimer.stop()
            toggleButtonEnabledStatus(true)
            lblInfo.text ="Click \'Run\' to start the pathfinder"
            drawing.repaint()
    }
    btnRun.addActionListener{
            graph.clearPath()
            pathfinder = AStar(graph)
            stepTimer.start()
            toggleButtonEnabledStatus(false)
            graph.isBeingTraversed = true
    }
    stepTimer.addActionListener {
        //tracks whether the pathfinder has finished searching, but also makes it execute its next step
        val runFinder : Boolean = pathfinder.nextStep()
        updateLabel(true)
        drawing.repaint()
        if (runFinder) {
            stepTimer.stop()
            toggleButtonEnabledStatus(true)
            updateLabel(false)
            graph.isBeingTraversed = false
        }
    }
}

/*
update the label to display info about he pathfinder's performance
param:
    whether the pathfinder is running
*/
fun updateLabel(isRunning : Boolean) {
    if (isRunning) {
        lblInfo.text = "<html>Running..."
    } else {
        lblInfo.text = "<html><div style='text-align: center;'>Search complete." + "<br/>" +
                "Distance: " + pathfinder.goal?.fDist + " </html>"
    }
}

/*
the buttons for selecting start, goal, walls, and empty nodes are all part of a buttongroup
we turn them all off when the pathfinder is running, and on when it's not
param:
    the state we want the buttons to have
 */
fun toggleButtonEnabledStatus(isOn : Boolean) {
    btnStart.isEnabled = isOn
    btnGoal.isEnabled = isOn
    btnWall.isEnabled = isOn
    btnErase.isEnabled = isOn
}

fun main(args: Array<String>) {
    buildUI()
    addButtonFunctions()
}