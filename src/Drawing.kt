import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.event.MouseAdapter
import java.awt.event.MouseEvent
import javax.swing.JPanel


/*
The action the drawing should commit when the user clicks or hold down on a node
Choose Start: clicked node will be the new start
Choose Goal: clicked node will be the new goal
Draw Wall: clicking and dragging will turn contacted nodes into walls
Erase: remove a wall
 */
enum class Action {
    CHOOSE_START, CHOOSE_GOAL, DRAW_WALL, ERASE
}


/*
A Frame is the container for the visual representation of the graph
It contains the visuals for all of the nodes
Params:
    the graph it will draw
    the size in pixels of the frame
 */
class Drawing(val graph: Graph, size: Int) : JPanel() {

    var nodeDrawSize = size / graph.size
    var currentAction : Action = Action.DRAW_WALL

    init {
        preferredSize = Dimension(size, size)
        /*
        this listener will take mouse input to decide when to draw the start/goal/walls
         */
        addMouseListener(object : MouseAdapter() {
            override fun mousePressed(e: MouseEvent) {
                if (!graph.isBeingTraversed) {
                    graph.clearPath()
                    //coordinates of node on graph calculated from coordinates of mouse
                    val nodeX = e.x / nodeDrawSize
                    val nodeY = e.y / nodeDrawSize
                    when (currentAction) {
                        Action.CHOOSE_START -> graph.setStart(nodeX, nodeY)
                        Action.CHOOSE_GOAL -> graph.setGoal(nodeX, nodeY)
                        Action.DRAW_WALL -> graph.setWall(nodeX, nodeY)
                        Action.ERASE -> graph.setEmpty(nodeX, nodeY)
                    }
                    repaint()
                }
            }
        })

        addMouseMotionListener(object : MouseAdapter() {
            override fun mouseDragged(e: MouseEvent) {
                if (!graph.isBeingTraversed) {
                    graph.clearPath()
                    val nodeX = e.x / nodeDrawSize
                    val nodeY = e.y / nodeDrawSize
                    when (currentAction) {
                        Action.DRAW_WALL -> graph.setWall(nodeX, nodeY)
                        Action.ERASE -> graph.setEmpty(nodeX, nodeY)
                    }
                    repaint()
                }
            }
        })
    }

    override fun paintComponent(g: Graphics?) {
        super.paintComponent(g)
        if (g != null) {
            //draw all the nodes
            for (column in graph.grid) {
                for (node in column) {
                    //give a grey outline
                    g.color = Color.LIGHT_GRAY
                    g.fillRect(node.x * nodeDrawSize, node.y * nodeDrawSize, nodeDrawSize, nodeDrawSize)
                    //color depends on type of node
                    when {
                        node.type == NodeType.EMPTY -> when {
                            node.isOnPath -> g.color = Color.YELLOW
                            node.wasChecked -> g.color = Color(255, 200, 220) //pink
                            else -> g.color = Color.WHITE
                        }
                        node.type == NodeType.WALL -> g.color = Color(130, 0, 205) //purple
                        node.type == NodeType.START -> g.color = Color(50, 150, 255) //light blue
                        node.type == NodeType.GOAL -> g.color = Color.GREEN
                    }
                    g.fillRect(node.x * nodeDrawSize + 1, node.y * nodeDrawSize + 1, nodeDrawSize - 2, nodeDrawSize - 2)
                }
            }
        }
    }
}