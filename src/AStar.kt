
import kotlin.math.pow
import kotlin.math.sqrt

//finds the euclidean distance (sqrt of sum of x and y distance) between start and end
//this is the heuristic function for the algorithm
fun euclidDist(start: Node, end: Node) : Float {
    return sqrt((end.x.toFloat() - start.x.toFloat()).pow(2) + (end.y.toFloat() - start.y.toFloat()).pow(2))
    }

/*
We put the A* algorithm into its own class, so that we can collect relevant variables and functions into one unified structure
Overview of the process
    2 sets of nodes: open (nodes we want to check first, initially containing the start node) and closed (nodes we already checked, initially empty)
    A node in the closed list can still appear in the open list if we have found a more efficient route to it)
    <--algorithm-->
    We look at the open node with the lowest f distance
        if it's the goal, then we're done
        Otherwise we look at each neighbor (we instantiate a clone of the neighbor and update its distance and parent properties to reflect the current path)
            If it's a wall, close it
            If there's an open node in the same location as it, compare its distances
                if it has a lower f distance, then we are on a suboptimal path and skip this neighbor
                if it has a higher/equal f distance, we are on an optimal path and remove the copy of this neighbor from the open list
            if there's a closed node in the same location as it, compare its distance
                if it has a lower f distance, then we are on a suboptimal path and skip this neighbor
            if we did not choose to skip this neighbor by this point, then add it to the open list to be examined later
    close this node
    repeat until the list of open nodes is empty
 */
class AStar(val graph: Graph) {

    //the nodes in the graph that we want to search next
    var openNodes : MutableList<Node> = mutableListOf(graph.start.clone())
    //the nodes that we know are not on the path
    var closedNodes : MutableList<Node> = mutableListOf()

    //the goal node, which will be assigned once we find it
    var goal : Node? = null



    /*
    check the next set of open nodes; only executed if openNodes is not empty
    looks at the open node that has the lowest fdist so far, and tries to find the most efficient next step towards the goal
    returns: the goal node (if it was reached), a default (-1, -1) node (if no path to the goal exists), or null (if it's not finished running)
     */
    fun checkNextOpenNode(): Node? {
        if (openNodes.isNotEmpty()) {
            //take the open node with the lowest fdist, as it has the highest chance of being on the optimal path
            val currentNode = openNodes.minBy { it.fDist } as Node
            openNodes.remove(currentNode)
            if (currentNode.type == NodeType.GOAL) return currentNode
            //check each neighbour to find the next step
            for (neighbour in currentNode.neighbours) {
                /*
                    we want multiple versions for the neighbour - each will have a different value of gdist
                    and hdist because they are accessed from a different path
                     */
                val successor = neighbour.clone()
                successor.previous = currentNode

                //don't consider walls
                if (successor.type == NodeType.WALL) {
                    /*
                        because the value of fdist for this node is initialized to 0 and has not been changed, the algorithm
                        will skip this node from now on
                         */
                    closedNodes.add(successor)
                    continue
                }

                /*
                    calculate fDist = g + h where
                    g = dist(start, currentNode) + dist(currentNode, successor)
                    h = heuristicDist(successor, goal)
                     */
                successor.gDist = currentNode.gDist + euclidDist(currentNode, successor)
                val hDist = euclidDist(successor, graph.goal)
                successor.fDist = successor.gDist + hDist

                /*
                    check if there is a copy of this node in the lists
                    if so, this means the pathfinder has explored other routes that have touched this node
                    we are concerned with whether that copy or this copy on the most efficient path
                    our implementation ensures there will never be more than 1 copy in the open nodes, but there might
                        be more than 1 in the closed nodes
                     */
                val openCopy = openNodes.find { it.x == successor.x && it.y == successor.y }
                if (openCopy != null) {/*
                        if the node in the list has a lower fDist than the one we have here, it means we currently are
                        on a suboptimal path, so skip this successor
                         */
                    if (openCopy.fDist < successor.fDist) {
                        continue
                    } else {
                        //otherwise, since we found a more efficient way to reach this node, delete the copy
                        openNodes.remove(openCopy)
                    }
                }
                val closedCopy = closedNodes.filter { it.x == successor.x && it.y == successor.y }.minBy { it.fDist }
                if (closedCopy != null) {
                    /*
                        if the node in the list has a lower fDist than the one we have here, it means we currently are
                        on a suboptimal path, so skip this successor
                         */
                    if (closedCopy.fDist < successor.fDist) {
                        continue
                    }
                }
                openNodes.add(successor)
            }
            //close this node
            closedNodes.add(currentNode)
            //get the actual copy of this node from the graph and mark it as checked
            graph.getNode(currentNode.x, currentNode.y).wasChecked = true

        } else {
            /*
            if the list of nodes is empty, then there is no path to the goal
            we return a node with coordinates (-1, -1) to show this
             */
            return Node(-1, -1, graph)
        }
        //returning null means we haven't found a path yet
        return null
    }

    /*
    carry out the next step in the pathfinding algorithm
    the algorithm is this:
        check the next open node, seeing if it leads to the goal
        tell canvas to draw to reflect the checked nodes
        repeat until we find the goal node
        once we find the goal node (or we know that there is no node), tell the canvas to draw
    returns: whether the algorithm has completed running
    */
    fun nextStep() : Boolean {
        if (goal == null) {
            goal = checkNextOpenNode()
            if (goal != null && goal!!.x == -1 && goal!!.y == -1) {//there is no path to the goal
                return true
            } else if (goal != null) {//there is a path to the goal
                //go through all nodes on the path and mark them as being on the path
                var previousStep: Node? = goal
                while (previousStep != null) {
                    graph.getNode(previousStep.x, previousStep.y).isOnPath = true
                    previousStep = previousStep.previous
                }
                return true
            }
        }
        return false
    }

}