Leo Huang 2020 

This app lets you see the A* algorithm find the optimal path from start to finish, step by step.  

You can move the start and end nodes and draw walls for the pathfinder to navigate around.  

the jar file is located in out/artifacts.  
Go to its directory and enter "java -jar 'Pathfinding Visualizer.jar'" on the command line.  